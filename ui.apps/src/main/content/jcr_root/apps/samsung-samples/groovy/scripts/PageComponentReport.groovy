import javax.jcr.query.Row
import javax.jcr.Node
import java.util.Calendar
import java.util.Date
import org.apache.commons.lang3.StringUtils
import org.apache.sling.api.resource.Resource
import java.nio.charset.StandardCharsets
import java.security.Key
import java.security.MessageDigest
import javax.crypto.Cipher
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.SecretKeySpec
import javax.xml.bind.annotation.adapters.HexBinaryAdapter
import com.day.cq.commons.jcr.JcrConstants
import java.text.SimpleDateFormat


// These paths should be updated to point to the appropriate content and component path(s)
def CONTENT_PATHS = ['/content/we-retail/us']
def COMPONENT_PATHS = ['/apps/weretail']

List<String> componentList = new ArrayList<>()
Map<String, List<String>> componentMap = new HashMap<>()
Map<String, List<String>> pageMap = new HashMap<>()

try {
	
    // get a list of all of the relevent components based on the entered path(s)
	for(String componentPath:COMPONENT_PATHS) {
	    if (session.nodeExists(componentPath)) {
			componentNode = getNode(componentPath)
			componentList = getComponentList(componentNode, componentList)
		}
	}
	
	// print a csv of the component paths based on the entered path(s)
	componentReport('ORIGINAL COMPONENT PATHS', componentList)
    
    // get a map of the page paths and components used based on the identified content path(s)
	for(String contentPath:CONTENT_PATHS) {
		if (session.nodeExists(contentPath)) {
			contentNode = getNode(contentPath)
			page = getPage(contentNode)
			pageMap = getPageMap(page, pageMap,componentList)
		}
	
	}
	
    // create the component map by iterating through the page map keys
	// and using each comopnent path as a key and the page path as a value in the value list
    for (Map.Entry<String,List<String>> entry : pageMap.entrySet())  {
	   pagePath = entry.getKey()
       List<String> pageComponentList = entry.getValue()
       for(String pageComponent:pageComponentList) {
           if(!componentMap.containsKey(pageComponent))  {
               List<String> pageList = new ArrayList<>()
               pageList.add(pagePath)
               componentMap.put(pageComponent,pageList)
           }
           else {
               if(!componentMap.get(pageComponent).contains(pagePath)) {
                   componentMap.get(pageComponent).add(pagePath)
               }
           }
       }
	}
	
    // create an unused components list by comparing the component map and the component list
    List<String> unusedComponents = getUnusedComponents(componentList,componentMap)

    // print out csv of the paths to unused components
    componentReport('UNUSED COMPONENT PATHS', unusedComponents)
   

    // print out csv of map with page paths as keys and components used by each page as a list of paths as values
    mapReport('PAGES WITH COMPONENTS', pageMap)
    
    
    // print out csv of map with component paths as keys and a list of page paths that use the components as values
    mapReport('COMPONENT USE ON PAGES', componentMap)
    
	
}catch(e){
  println e
}

// get the list of components and create an initial map
// with the component path and an empty list where the pages will be added later
def getComponentList(node, componentList) {
    node.recurse {
        innerNode -> 
            if(isComponent(innerNode))  {
                componentList.add(innerNode.getPath())
            }
        
    }
    componentList
}

// get a map of the pages with the page path as the key and a list of the component paths as the value
def getPageMap(page,pageMap,componentList) {
	page.recurse{
	    innerPage ->
	        List<String> pageComponentList = new ArrayList<>()
	        innerPagePath = innerPage.getPath()
	        innerPageNode = getNode(innerPagePath)
	        
	        innerPageNode.recurse {
	            innerComponentNode ->
    	            componentPath = innerComponentNode.hasProperty('sling:resourceType') ? '/apps/' + innerComponentNode.getProperty('sling:resourceType').getString() : StringUtils.EMPTY
    	            if(!pageComponentList.contains(componentPath) && componentList.contains(componentPath))   {
    	               pageComponentList.add(componentPath)
    	            }
	        }
	        pageMap.put(innerPagePath,pageComponentList)
	       
	} 
	pageMap
}

def getPage(node) {
  pagePath = node.getPath()
  pageResource = resourceResolver.getResource(pagePath)
  page = pageManager.getContainingPage(pageResource)
  page
}

def isComponent(node) {
    isComponent = false
    jcrPrimaryType = node.hasProperty('jcr:primaryType') ? node.getProperty('jcr:primaryType').getString() : StringUtils.EMPTY
    if(jcrPrimaryType.equals('cq:Component'))   {
        isComponent = true
    }
    isComponent
}

def getUnusedComponents(componentList, componentMap)    {
    List<String> unusedComponents = new ArrayList<>()
    for(String component:componentList)  {
        if(!componentMap.containsKey(component)) {
            unusedComponents.add(component)
        }
    }
    unusedComponents
}

def componentReport(heading, list) {
    println heading
    for(String item:list) {
        println item
    }
    println ''
}


def mapReport(heading, map) {
    println heading
    for (Map.Entry<String,List<String>> entry : map.entrySet())  {
        println entry.getKey() + ',' + entry.getValue().join(',')
    }
    println ''
}
